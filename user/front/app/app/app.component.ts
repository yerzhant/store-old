import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { HTTP_PROVIDERS } from '@angular/http';

// import 'rxjs/Rx'; // load the full rxjs

@Component({
    selector: 'app',
    templateUrl: './app/app/app.component.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS]
})
export class AppComponent { }
