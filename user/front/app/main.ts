import { enableProdMode } from '@angular/core';
import { bootstrap } from '@angular/platform-browser-dynamic';

import { AppComponent } from './app/app.component';
import { appRouterProviders } from './app/app.routes';

// enableProdMode();

bootstrap(AppComponent, [
    appRouterProviders
]).catch(error => console.log(error));
