package kz.store.entity.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.store.StoreApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StoreApplication.class)
@WebAppConfiguration
public class AppRolesTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testSizeAndName() throws Exception {
        mvc.perform(get("/api/appRoles"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.appRoles.length()", greaterThan(0)))
                .andExpect(jsonPath("$._embedded.appRoles[0].name", equalTo("Test")));
    }

    @Test
    public void testCreate() throws Exception {
        AppRole ar = new AppRole();
        ar.setName("Test");
        ar.setCode("TEST");

        mvc.perform(post("/api/appRoles")
                .content(new ObjectMapper().writeValueAsBytes(ar)))
                .andExpect(status().isCreated());
    }
}
