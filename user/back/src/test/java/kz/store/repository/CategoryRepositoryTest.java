package kz.store.repository;

import java.util.List;
import kz.store.StoreApplication;
import kz.store.entity.Category;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StoreApplication.class)
@Transactional
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository r;

    Category c;

    @Before
    public void setUp() {
        c = new Category();
        c.setName("Test");
        r.save(c);
    }

    @Test
    public void testFindAll() {
        List<Category> l = r.findAll();
        assertTrue(l.size() > 0);
    }

    @Test
    public void testFindOne() {
        Category c2 = r.findOne(c.getId());
        assertEquals(c, c2);
    }

    @Test
    public void testCreate() {
        assertNotNull(c.getId());
    }

    @Test
    public void testUpdate() {
        Category c2 = r.findOne(c.getId());
        c2.setName("New test");
        r.save(c2);
        Category c3 = r.findOne(c2.getId());
        assertEquals(c2, c3);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetNullName() {
        Category c2 = new Category();
        c2.setName(null);
        r.save(c2);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetEmptyName() {
        Category c2 = new Category();
        c2.setName("");
        r.save(c2);
    }

    @Test
    public void testParent() {
        Category c2 = new Category();
        c2.setName("Child");
        c2.setParent(c);
        r.save(c2);
        Category c3 = r.findOne(c2.getId());
        assertEquals(c, c3.getParent());
    }

    @Test
    public void testDelete() {
        r.delete(c);
        Category c2 = r.findOne(c.getId());
        assertNull(c2);
    }
}
