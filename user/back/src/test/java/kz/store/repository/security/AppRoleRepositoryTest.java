package kz.store.repository.security;

import kz.store.StoreApplication;
import kz.store.entity.security.AppRole;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StoreApplication.class)
@Transactional
public class AppRoleRepositoryTest {

    @Autowired
    private AppRoleRepository r;

    private AppRole ar;

    @Before
    public void setUp() {
        ar = new AppRole();
        ar.setName("Test");
        ar.setCode("TEST");
        r.save(ar);
    }

    @Test
    public void testCreate() {
        assertNotNull(ar.getId());
    }

}
