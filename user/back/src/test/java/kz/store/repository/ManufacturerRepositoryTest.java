package kz.store.repository;

import java.util.List;
import kz.store.StoreApplication;
import kz.store.entity.Manufacturer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = StoreApplication.class)
@Transactional
public class ManufacturerRepositoryTest {

    @Autowired
    private ManufacturerRepository r;

    Manufacturer m;

    @Before
    public void setUp() {
        m = new Manufacturer();
        m.setName("Test");
        r.save(m);
    }

    @Test
    public void testFindAll() {
        List<Manufacturer> l = r.findAll();
        assertTrue(l.size() > 0);
    }

    @Test
    public void testFindOne() {
        Manufacturer m2 = r.findOne(m.getId());
        assertEquals(m, m2);
    }

    @Test
    public void testCreate() {
        assertNotNull(m.getId());
    }

    @Test
    public void testUpdate() {
        Manufacturer m2 = r.findOne(m.getId());
        m2.setName("New test");
        r.save(m2);
        Manufacturer m3 = r.findOne(m2.getId());
        assertEquals(m3, m2);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetNullName() {
        Manufacturer m2 = new Manufacturer();
        m2.setName(null);
        r.save(m2);
    }

    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSetEmptyName() {
        Manufacturer m2 = new Manufacturer();
        m2.setName("");
        r.save(m2);
    }

    @Test
    public void testDelete() {
        r.delete(m);
        assertNull(r.findOne(m.getId()));
    }
}
