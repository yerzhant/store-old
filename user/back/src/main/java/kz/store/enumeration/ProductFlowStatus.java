package kz.store.enumeration;

public enum ProductFlowStatus {

    NEW, CANCELLED, ACCEPTED, REJECTED, PAID;
}
