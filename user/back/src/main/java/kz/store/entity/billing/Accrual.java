package kz.store.entity.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import lombok.Data;

@Entity
@Data
public class Accrual extends BaseEntity {

    @NotNull
    private LocalDateTime accruedOn;

    @NotNull
    private BigDecimal amount;

    @NotNull
    @ManyToOne
    private Contract contract;
}
