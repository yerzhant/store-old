package kz.store.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import kz.store.entity.billing.Store;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class Category extends BaseEntity {

    @NotEmpty
    private String name;

    @ManyToOne
    private Category parent;

    @NotNull
    @ManyToOne
    private Store store;
}
