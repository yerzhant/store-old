package kz.store.entity.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import lombok.Data;

@Entity
@Data
public class Payment extends BaseEntity {

    @NotNull
    private LocalDateTime paidOn;

    @NotNull
    private BigDecimal amount;

    @NotNull
    @ManyToOne
    private Contract contract;

    @NotNull
    @ManyToOne
    private PaymentChannel paymentChannel;

    private String description;
}
