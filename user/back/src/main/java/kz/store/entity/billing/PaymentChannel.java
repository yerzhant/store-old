package kz.store.entity.billing;

import javax.persistence.Entity;
import kz.store.entity.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class PaymentChannel extends BaseEntity {

    @NotEmpty
    private String name;

    private String code;
}
