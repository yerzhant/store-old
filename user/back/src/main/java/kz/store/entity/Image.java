package kz.store.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
public class Image extends BaseEntity {

    @NotNull
    private byte[] buffer;

    private String description;

    @NotNull
    @ManyToOne
    private Product product;
}
