package kz.store.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import kz.store.entity.billing.Store;
import kz.store.enumeration.ProductFlowStatus;
import lombok.Data;

@Entity
@Data
public class ProductFlow extends BaseEntity {

    @NotNull
    private LocalDateTime flowedOn = LocalDateTime.now();

    @NotNull
    private Boolean isInward;

    @NotNull
    private ProductFlowStatus status = ProductFlowStatus.NEW;

    @ManyToOne
    private Supplier supplier;

    @ManyToOne
    private Client client;

    @NotNull
    @ManyToOne
    private Store store;
}
