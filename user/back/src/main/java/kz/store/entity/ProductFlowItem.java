package kz.store.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
public class ProductFlowItem extends BaseEntity {

    @NotNull
    @Min(1)
    private Integer quantity;

    @DecimalMin("0")
    private BigDecimal price;

    @NotNull
    @ManyToOne
    private Product product;

    @NotNull
    @ManyToOne
    private ProductFlow productFlow;
}
