package kz.store.entity.billing;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import kz.store.entity.BaseEntity;
import lombok.Data;

@Entity
@Data
public class Contract extends BaseEntity {

    @NotNull
    private LocalDate beginDate;

    private LocalDate endDate;

    @NotNull
    @DecimalMin("0")
    private BigDecimal initialFee;

    @NotNull
    @DecimalMin("0")
    private BigDecimal monthlyFee;

    @NotNull
    @ManyToOne
    private Store store;
}
