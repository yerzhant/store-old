package kz.store.repository.billing;

import kz.store.entity.billing.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store, Long> {

}
