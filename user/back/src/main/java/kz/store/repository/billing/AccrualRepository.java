package kz.store.repository.billing;

import kz.store.entity.billing.Accrual;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccrualRepository extends JpaRepository<Accrual, Long> {

}
